({
    doInit: function(component, event, helper) {  
       var page = component.get("v.page") || 1;
       var recordToDisplay = component.find("recordSize").get("v.value"); 
       helper.getImages(component, page, recordToDisplay);
  
    },
  
    navigate: function(component, event, helper) { 
       var page = component.get("v.page") || 1;
       var direction = event.getSource().get("v.label");
       var filter = '';
       var recordToDisplay = component.find("recordSize").get("v.value"); 
       page = direction === "Previous Page" ? (page - 1) : (page + 1);
       filter = component.find("SearchBox").get("v.value");
       if(filter==''){
         helper.getImages(component, page, recordToDisplay);
       }
       else{

         helper.getFilteredImages(component, page, recordToDisplay, filter);
       }

  
    },

    searchTable : function(component, event, helper) {
        var page = component.get("v.page") || 1;   
        var recordToDisplay = component.find("recordSize").get("v.value");
        var filter = component.find("SearchBox").get("v.value");
        helper.getFilteredImages(component, page, recordToDisplay, filter);
    },

    sendMail : function(component, event, helper) {
        var filter = component.find("SearchBox").get("v.value");
        var mail = component.find("MailBox").get("v.value");
        console.log(mail);

        helper.sendFilteredImages(component, filter, mail);
    },
  
    onSelectChange: function(component, event, helper) {
       var page = 1
       var recordToDisplay = component.find("recordSize").get("v.value");
       helper.getImages(component, page, recordToDisplay);
    },
  
 })
