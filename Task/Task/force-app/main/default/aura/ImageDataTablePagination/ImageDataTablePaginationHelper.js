({
    getImages: function(component, page, recordToDisplay) {
  

       var action = component.get("c.fetchImage");
       action.setParams({
          "pageNumber": page,
          "recordToDisplay": recordToDisplay
       });

       action.setCallback(this, function(a) {
          var result = a.getReturnValue();
          console.log('result ---->' + JSON.stringify(result)); 
  
          component.set("v.Images", result.images);
          component.set("v.page", result.page);
          component.set("v.total", result.total);
          component.set("v.pages", Math.ceil(result.total / recordToDisplay));
  
       });

       $A.enqueueAction(action);
    },

    getFilteredImages: function(component, page, recordToDisplay, filter) {
  
        var action = component.get("c.fetchImage2");
        action.setParams({
           "pageNumber": page,
           "recordToDisplay": recordToDisplay,
           "filter": filter
        }); 
        action.setCallback(this, function(a) {

           var result = a.getReturnValue();
 
           component.set("v.Images", result.images);
           component.set("v.page", result.page);
           component.set("v.total", result.total);
           component.set("v.pages", Math.ceil(result.total / recordToDisplay));
   
        });

        $A.enqueueAction(action);
     },

     
    sendFilteredImages: function(component, searchBox, mail) {
  
        var action = component.get("c.sendImages");
        action.setParams({
           "search": searchBox,
           "mail": mail

        });
  
        action.setCallback(this, function(a) {
           var result = a.getReturnValue();
           console.log('result ---->' + JSON.stringify(result));  
   
   
        });

        $A.enqueueAction(action);
     }


 })
