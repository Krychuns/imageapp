public class ImageAppTestHelper {

    public static StaticResourceCalloutMock setImageApiMockForTests(){
        StaticResourceCalloutMock mock = new StaticResourceCalloutMock();
        mock.setStaticResource('imageApiResponse');
        mock.setStatusCode(200);
        mock.setHeader('Content-Type', 'application/json');
        return mock;
    }



}
