@isTest
public class ImageAppTest {

    @isTest
    public static void testProcessinApiResponseInsertAndDelete(){

        Image__c testImage = new Image__c();
        testImage.ImageId__c = 2222;
        insert testImage;
        
        Test.startTest();
        StaticResourceCalloutMock mock = ImageAppTestHelper.setImageApiMockForTests();
        Test.setMock(HttpCalloutMock.class, mock);
        ImagesService.getImagesFromApi();
        Test.stopTest();

        List<Image__c> allImages = [SELECT id FROM Image__c];
        System.assertEquals(allImages.size(),3);

    }

    @isTest
    public static void testProcessinApiResponseOnlyInsert(){
   
        Test.startTest();
        StaticResourceCalloutMock mock = ImageAppTestHelper.setImageApiMockForTests();
        Test.setMock(HttpCalloutMock.class, mock);
        ImagesService.getImagesFromApi();
        Test.stopTest();

        List<Image__c> allImages = [SELECT id FROM Image__c];
        
        System.assertEquals(allImages.size(),3);

    }

    @isTest
    public static void testProcessinApiResponseUpsert(){

        Image__c testImage = new Image__c();
        testImage.ImageId__c = 1;
        insert testImage;
        
        Test.startTest();
        StaticResourceCalloutMock mock = ImageAppTestHelper.setImageApiMockForTests();
        Test.setMock(HttpCalloutMock.class, mock);
        ImagesService.getImagesFromApi();
        Test.stopTest();

        List<Image__c> allImages = [SELECT id, ImageId__c, title__c FROM Image__c ORDER BY ImageId__c];
        
        System.assertEquals(allImages.size(),3);
        System.assertNotEquals(allImages[0],null);

    }

    @isTest
    public static void testImageSyncBatchCreation(){

        Test.startTest();
        StaticResourceCalloutMock mock = ImageAppTestHelper.setImageApiMockForTests();
        Test.setMock(HttpCalloutMock.class, mock);
        SyncImages.setSchedule('0 0 0,12 ? * * *', 'testImageSyncBatch');
        Test.stopTest();

        List<AsyncApexJob> apexJobs = [SELECT ApexClassId, Id FROM AsyncApexJob WHERE JobType IN ('BatchApexWorker','ScheduledApex')];

        System.assertNotEquals(apexJobs.size(),0);

    }
    @isTest
    public static void testImageMailing(){

    Image__c testImage = new Image__c();
    testImage.ImageId__c = 1;
    testImage.Title__c = 'test';
    insert testImage;    

    Test.startTest();
    ImagePaginationController.sendImages('test', 'krystianlipiec@gmail.com');   
    Integer invocations = Limits.getEmailInvocations(); 
    Test.stopTest();

    system.assertEquals(1, invocations);

    }
    

}
