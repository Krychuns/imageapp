public with sharing class ImagesService {
 
    
    @future(callout=true)
    public static void getImagesFromApi(){

        HTTP httpCall = new HTTP();
        HTTPRequest request = new HTTPRequest();

        request.setEndpoint('https://jsonplaceholder.typicode.com/photos');
        request.setMethod('GET');

        HTTPResponse response = new HTTPResponse();
        response = httpCall.send(request);
        
        processApiResponse(response.getBody());

    }

    public static void processApiResponse(String responseBody){
        
        List<Image> newImages = (List<Image>) JSON.deserialize(responseBody, List<Image>.class);
        Map<Decimal,Image__c> allImages = ImageSelector.selectExistingImagesAsMap();
        
        Map<Decimal,Image__c> importedImages = new Map<Decimal,Image__c>();
        List<Image__c> imagesMarkedForDeletion = new List<Image__c>();

        importedImages = getImagesForUpsert(newImages, allImages);
             
        imagesMarkedForDeletion = getImagesForDeletion(importedImages, allImages);
        //^ or just 
        //imagesMarkedForDeletion = ImageSelector.selectImagesForDeletion(importedImages.values());
           
        upsert importedImages.values();
        delete imagesMarkedForDeletion;
    
    }

    public static Map<Decimal,Image__c> getImagesForUpsert(List<Image> newImages, Map<Decimal,Image__c> allImages){
        
        Map<Decimal,Image__c> newSObjectImages = new Map<Decimal,Image__c>();

        for(Image image : newImages){
            if(allImages.get(image.id)!=null){
                Image__c currentlyExistingImage = allImages.get(image.id);
                currentlyExistingImage = updateSObjectImage(image, currentlyExistingImage);
                newSObjectImages.put(image.id, currentlyExistingImage); 
            }
            else{               
                newSObjectImages.put(image.id, createNewSObjectImage(image)); 
            }

        }
        
      return newSObjectImages;  

    }

    public static List<Image__c> getImagesForDeletion(Map<Decimal,Image__c> importedImages, Map<Decimal,Image__c> allImages){
        
        List<Image__c> unnecessarySObjectImages = new List<Image__c>();
        
        for(Decimal imageId : allImages.keySet()){
            if(!importedImages.keySet().contains(imageId)){
                Image__c tempImage = allImages.get(imageId);
                unnecessarySObjectImages.add(tempImage);
            }
        }

        return unnecessarySObjectImages;
    
    }

    public static Image__c createNewSObjectImage(Image image){
        
        Image__c customImage = new Image__c();
        customImage.album_id__c = image.albumid;
        customImage.imageid__c = image.id;
        customImage.title__c = image.title;
        customImage.url__c = image.url;
        customImage.thumbnail_url__c = image.thumbnailurl;

        return customImage;
    }

    public static Image__c updateSObjectImage(Image image, Image__c customImage){
        
        customImage.album_id__c = image.albumid;
        customImage.title__c = image.title;
        customImage.url__c = image.url;
        customImage.thumbnail_url__c = image.thumbnailurl;

        return customImage;
    }

}

