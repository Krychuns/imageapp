public with sharing class ImageSelector {

        public static Map<Decimal,Image__c> selectExistingImagesAsMap (){
            List<Image__c> images = [SELECT id, album_id__c, imageid__c, title__c, thumbnail_url__c, url__c FROM Image__c];
            Map<Decimal, Image__c> imagesMap = new Map<Decimal, Image__c>();
            for(Image__c image : images){
                imagesMap.put(image.imageid__c,image);
            }

            return imagesMap;

        }

        public static List<Image__c> selectImagesForDeletion(List<Image__c> insertedImages){
            List<Image__c> imagesForDeletion = [SELECT id FROM Image__c WHERE id NOT IN : insertedImages];
            return imagesForDeletion;
        }




}
