global with sharing class SyncImages implements Schedulable {
    global void execute(SchedulableContext sc){
        doBatchWork();
    }

    public static void doBatchWork(){
        ImagesService.getImagesFromApi();
    }
    //example daily cron expression '0 0 0,12 ? * * *'
    //to create schedulable job call SyncImages.setSchedule('0 0 0,12 ? * * *', 'SyncImageBatch'); in execute anonymous window
    global static String setSchedule(String scheduleTime, String scheduleName){
        SyncImages syncImagesScheduler = new SyncImages();
        return System.schedule(scheduleName, scheduleTime, syncImagesScheduler);
    }

}
