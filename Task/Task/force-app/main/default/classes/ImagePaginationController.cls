public with sharing class ImagePaginationController {
   @AuraEnabled
   public static ImagePagerWrapper fetchImage(Decimal pageNumber ,Integer recordToDisplay) {
    Integer pageSize = recordToDisplay;
    Integer offset = ((Integer)pageNumber - 1) * pageSize;
  

  ImagePagerWrapper obj =  new ImagePagerWrapper();
      obj.pageSize = pageSize;
      obj.page = (Integer) pageNumber;
      obj.total = [SELECT count() FROM Image__c];
      obj.images = [SELECT Id, Title__c, Album_id__c, ImageId__c, Url__c FROM Image__c ORDER BY Title__c LIMIT :recordToDisplay OFFSET :offset];

      return obj;
   }
   @AuraEnabled
   public static ImagePagerWrapper fetchImage2(Decimal pageNumber ,Integer recordToDisplay, String filter) {
    Integer pageSize = recordToDisplay;
    Integer offset = ((Integer)pageNumber - 1) * pageSize;
    String searchString = '%' + filter + '%';

  ImagePagerWrapper obj =  new ImagePagerWrapper();
      obj.pageSize = pageSize;
      obj.page = (Integer) pageNumber;
      obj.images = [SELECT Id, Title__c, Album_id__c, ImageId__c, Url__c FROM Image__c WHERE Title__c LIKE :searchString ORDER BY Title__c LIMIT :recordToDisplay OFFSET :offset];
      obj.total = [SELECT COUNT() FROM Image__c WHERE Title__c LIKE :searchString];

      return obj;
   }

   @AuraEnabled
   public static void sendImages(String search, String mail) {
    String searchString = '%' + search + '%';
    List<Image__c> images = [SELECT Title__c, Album_id__c, ImageId__c, Url__c FROM Image__c WHERE Title__c LIKE :searchString ORDER BY Title__c];
    String body = '';
    Integer i = 0;
    for(Image__c image : images){
        i++;
        body += String.ValueOf(i) + '. ' + image.Title__c + ' ' + image.Album_id__c + ' ' + image.Url__c +'<br/>';
    }

    Messaging.SingleEmailMessage singleMail = new Messaging.SingleEmailMessage();
    String[] toAddresses = new String[]{};
    toAddresses.add(mail);   
    singleMail.setToAddresses(toAddresses);
    singleMail.setSenderDisplayName('Salesforce Image Task');
    singleMail.setSubject('Imported images');
    singleMail.setHtmlBody(body);
    Messaging.sendEmail(new Messaging.SingleEmailMessage[] { singleMail });
   }
     
 public class ImagePagerWrapper {
    @AuraEnabled public Integer pageSize {get;set;}
    @AuraEnabled public Integer page {get;set;}
    @AuraEnabled public Integer total {get;set;}
    @AuraEnabled public List<Image__c> images {get;set;}
   }
}